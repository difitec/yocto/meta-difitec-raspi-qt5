DESCRIPTION = "A Qt5 image for Raspberry-Pi made by DIFITEC"
LICENSE = "MIT"

inherit core-image

inherit populate_sdk_qt5_ext

require recipes-core/images/core-image-base.bb

IMAGE_INSTALL:append = " \
    dhcpcd \
    gdb gdbserver \
    tslib tslib-calibrate tslib-tests \
    ttf-bitstream-vera liberation-fonts \
    qtbase qtbase-plugins \
    qtdeclarative \
    qtquickcontrols qtquickcontrols-qmlplugins \
    qtquickcontrols2 qtquickcontrols2-qmlplugins \
    qtgraphicaleffects qtimageformats qtmultimedia \
    qtserialport \
    "

# add feature SSH server
EXTRA_IMAGE_FEATURES:append = " ssh-server-dropbear"

# add packages nano and mc
CORE_IMAGE_EXTRA_INSTALL:append = " nano mc"

##### section of extra IoT features (comment out if not required) #####

# add MQTT broker mosquitto
CORE_IMAGE_EXTRA_INSTALL:append = " mosquitto"

# add MQTT libraries and client tools
IMAGE_INSTALL:append = " libmosquitto1 libmosquittopp1 mosquitto-clients"

# install node-red
IMAGE_INSTALL:append = " node-red"
