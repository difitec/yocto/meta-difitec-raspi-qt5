# meta-difitec-raspi-qt5

## Name

Yocto - Embedded Linux + Qt-Framework für Raspberry-Pi4 64 Bit

## Beschreibung

Diese Website enthält einen Yocto Meta Layer für eine konkrete Abbildung eines Embedded Linux + Qt-Framework auf einer Raspberry-Pi4 Hardware mit 7 Zoll Touch Display.

Das Linux Betriebssystem kann hierbei als 64 Bit Version ausgerollt werden, da der Raspberry-Pi4 mit der CPU Broadcom BCM2711 mit Quad-Core Cortex-A72 (ARM v8) eine 64 Bit Prozessor-Architektur besitzt.

Das Projekt dient hauptsächlich als Anleitung und Dokumentation für den generellen Umgang mit Yocto und dem Build System bitbake, erstellt anderseits jedoch auch ein lauffähiges System für den Raspberry-Pi4, welches für Projekte genutzt werden kann.

Die im Wiki befindliche Dokumentation erklärt ausführlich Schritt-für-Schritt, wie man mit Yocto ein maßgeschneidertes Embedded Linux Betriebssystem mit integriertem Qt-Framework für grafische Bedieneroberflächen für eine Hardware-Zielplattform konfiguriert.

Im Prinzip kann diese Anleitung auf jede andere verfügbare Hardware adaptiert werden und funktioniert genauso auch für 32 Bit Architekturen wie z.B. dem NXP i.MX6.

Diese Dokumentation konzentriert sich auf die OpenSource Konfiguration, d.h. eine Strategie, Linux und Qt-Framework unter OpenSource Lizenz Bedingungen entweder privat oder auch kommerziell nutzen zu können.

Die unter kommerzieller Lizenz stehenden Varianten wie Boot-To-Qt werden hier nicht besprochen.

## Autor

Ulf Schönherr

Dieses Projekt wird bereitgestellt durch DIFITEC GmbH, Dresden, Germany.

## Lizenz

[MIT Lizenz](https://opensource.org/licenses/MIT)

Copyright 2023 DIFITEC GmbH

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Projekt Status

aktiv
